FROM nginx:1.17.5-alpine
LABEL maintainer="MrFlutters (https://github.com/MrFlutters)"
COPY docker/nginx.conf /etc/nginx/nginx.conf
COPY docker/web.conf /etc/nginx/conf.d/web.conf

EXPOSE 80
RUN rm -Rf /usr/share/nginx/html/ && rm /etc/nginx/conf.d/default.conf

# Big files, that are rarely updated/changed
# This is done to optimize docker caching aswell as build speed
COPY src/HORSECRAFT-GNU.zip /usr/share/nginx/html/
COPY src/HORSECRAFT-NT.zip /usr/share/nginx/html/
COPY src/mods.7z /usr/share/nginx/html/

# Lighter files that are more often changed
COPY ["src/packer", "src/PackUpdaterWin/", "/usr/share/nginx/html/"]
COPY ["src/version", "src/versionw", "/usr/share/nginx/html/"]
COPY ["src/list", "src/listOld", "src/listserver", "/usr/share/nginx/html/"]

CMD [ "nginx", "-g", "daemon off;" ]
